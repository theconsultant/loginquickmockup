﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LoginMockUp
{
    public partial class ProdLogin : System.Web.UI.Page
    {
        //This mock up has 3 hardcoded users

        //User 1:
        //  userId: 1
        //	Username: oribe.peralta@prod-design.com
        //	password: password
        //	rol: employee
        //	manager: juan carlos osorio
		//	Department: Design
		//	Employee#: 90000012
		//	picURL: https://i.ebayimg.com/images/g/~~sAAOSwbsBXidvJ/s-l300.jpg
        //	
        //User 2:
        //  UserId: 2
        //	Username: carlos.vela@prod-design.com
        //	password: password
        //	rol: employee
        //	manager: ricardo ferreti
		//	Department: Design
		//	Employee#: 90000013
		//	picURL: https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ6LJey34fGJBqKhzW9pm3Oo17wSTsyqz3ThZ4_DT99qc571Hnp

        //User 3:
        //  UserID: 3
        //	Username: jc.osorio@prod-design.com
        //	password: password
        //	rol: manager
        //	manager: enrique pena nieto
		//	Department: Design
		//	Employee#: 90000002
		//	picURL: https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS0Nu9sGziWF0rAQ4whflgZtC2FlHSxkqid33OADEghWXn7fOXy

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void cmd_login_Click(object sender, EventArgs e)
        {
            if(String.IsNullOrEmpty(txt_username.Text) || String.IsNullOrEmpty(txt_password.Text))
            {
                lbl_error.Text = "Type username and password.";
                lbl_error.Visible = true;
                return;
            }

            int userid = 0;
            bool validTestUser = false;
            switch(txt_username.Text.ToUpper())
            {
                case "ORIBE.PERALTA@PROD-DESIGN.COM":
                    validTestUser = true;
                    userid = 1;
                    break;
                case "CARLOS.VELA@PROD-DESIGN.COM":
                    validTestUser = true;
                    userid = 2;
                    break;
                case "JC.OSORIO@PROD-DESIGN.COM":
                    validTestUser = true;
                    userid = 3;
                    break;
                default:
                    validTestUser = false;
                    break;
            }

            if(!validTestUser)
            {
                lbl_error.Text = "Invalid User Name.";
                lbl_error.Visible = true;
                return;
            }

            if(txt_password.Text != "password")
            {
                lbl_error.Text = "Invalid Password.";
                lbl_error.Visible = true;
                return;
            }

            lbl_error.Text = "Redirect with User ID " + userid;
            lbl_error.Visible = true;

            Response.Redirect("http://localhost/TimeSheet.asp?userID=" + userid, true);
        }

        protected void cmd_cancel_Click(object sender, EventArgs e)
        {
            txt_password.Text = "";
            txt_username.Text = "";
            lbl_error.Text = "";
            lbl_error.Visible = false;
        }
    }
}