﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ProdLogin.aspx.cs" Inherits="LoginMockUp.ProdLogin" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <span style="font-size: x-large; align-content:center">
    <br />
        
    Welcome to Prod Design

    </span> <br />
    <span style="font-size: large">Please login to the system
    <br />
    <br />
    </span>
    <table>
        <tr>
            <td>
                Username:
            </td>
            <td colspan="2">
                <asp:TextBox ID="txt_username" runat="server" Width="230px" ToolTip="Input User Name"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                Password:
            </td>
            <td colspan="2">
                <asp:TextBox ID="txt_password" runat="server" TextMode="Password" Width="230px" ToolTip="Input Password"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Label ID="lbl_error" runat="server" Visible="false" ForeColor="Red" Font-Bold="true"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>

            </td>
            <td>
                <asp:Button ID="cmd_login" runat="server" Text="Login" OnClick="cmd_login_Click" />
            </td>
            <td>
                <asp:Button ID="cmd_cancel" runat="server" Text="Cancel" OnClick="cmd_cancel_Click" />
            </td>
        </tr>
        <tr>
            <td>

            </td>
            <td>
                <asp:HyperLink ID="url_forgotpwd" runat="server" Text="Forgot your password?"></asp:HyperLink>
            </td>
        </tr>
    </table>
</asp:Content>
